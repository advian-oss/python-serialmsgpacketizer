#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec serialmsgpacketizer -vv docker_config.toml
else
  exec "$@"
fi
